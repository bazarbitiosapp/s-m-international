//
//  AIValidationManager.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

class PIValidation: NSObject {
    class public func isBlankString(str: String) -> Bool {
        
        if str == "" {
            return true
        } else {
            return false
        }
        
    }
    
    class public func isNumberString(str: String) -> Bool {
        
        if str.isNumber() {
            return true
        } else {
            return false
        }
        
    }
    
    class public func isCheckLength(str: String, length: Int) -> Bool {
        
        let passLength = str.characters.count
        
        if passLength < length {
            return true
        } else {
            return false
        }
    }
    
    class public func isCompareTwoStringEqual(firstStr: String, secondStr: String) -> Bool {
        
        if firstStr != secondStr {
            return true
        } else {
            return false
        }
        
    }
    
    // For abc@.gmail.com
    class public func isEmailString(str: String) -> Bool    {
        
        let isValid1 = isValidEmail(str)
        let isvalid2 = isValidEmail_NEW(str)
        
        print("1: \(isValid1)")
        print("2: \(isvalid2)")
        
        if isValid1 == true && isvalid2 == true {
            return false
        } else {
            return true
        }
        
    }
    
    class private func isValidEmail(_ stringToCheckForEmail:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z]+([._%+-]{1}[A-Z0-9a-z]+)*@[A-Z0-9a-z]+([.-]{1}[A-Z0-9a-z]+)*(\\.[A-Za-z]{2,4}){0,1}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: stringToCheckForEmail)
        
    }
    
    class private func isValidEmail_NEW(_ stringToCheckForEmail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringToCheckForEmail)
        
    }
}





