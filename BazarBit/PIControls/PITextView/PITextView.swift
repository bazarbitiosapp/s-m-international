//
//  PITextView.swift
//  TabInsta
//
//  Created by Sangani ajay on 9/14/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import Foundation

class PICustomTextView: UITextView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor.lightGray
        self.font = UIFont.appFont_Light_WithSize(16)
    }
}
