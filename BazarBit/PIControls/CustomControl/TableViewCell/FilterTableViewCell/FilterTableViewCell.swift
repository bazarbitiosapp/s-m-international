//
//  FilterTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/27/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var viewBack : UIView!
    @IBOutlet var labelTitle: PIFilterTitleLightLabel!
    @IBOutlet var filterTypeCollectionView: UICollectionView!
    
    var objFilterVC : FilterViewController!
    var isShowRecord : Bool = false
    var identifier : String = "filterTypeCell"
    var aryAttributedData : [AnyObject] = [AnyObject]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        filterTypeCollectionView.delegate = self
        filterTypeCollectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: setUpData
    func setUpData(arrData : [AnyObject], strName : String) {
        self.labelTitle.text = strName
        self.aryAttributedData = arrData
        filterTypeCollectionView.reloadData()
    }
    
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedCell : FilterTypeCollectionViewCell = collectionView.cellForItem(at: indexPath) as! FilterTypeCollectionViewCell
        
        let dict : [String : AnyObject] = aryAttributedData[indexPath.row] as! [String : AnyObject]
        
        selectedCell.isSelect = !selectedCell.isSelect
        
        objFilterVC.isClear = false
        
        if selectedCell.isSelect == true {
            //selectedCell.labelTitle.backgroundColor = UIColor(red: 62.0/255.0, green: 62.0/255.0, blue: 62.0/255.0, alpha: 1.0)
            selectedCell.labelTitle.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
            selectedCell.labelTitle.textColor = UIColor.white
            
            //APPDELEGATE.arrSelect.append(dict["attributeterm_id"]! as! String)
            APPDELEGATE.arrSelect.append(dict["z_attributetermid_pk"]! as! String)
            print(APPDELEGATE.arrSelect)
        }
        else
        {
            selectedCell.labelTitle.setTextLabelDynamicTextColor()
            selectedCell.labelTitle.backgroundColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
            
            for i in 0...APPDELEGATE.arrSelect.count - 1 {
                if APPDELEGATE.arrSelect[i] == dict["z_attributetermid_pk"]! as! String {
                    APPDELEGATE.arrSelect.remove(at: i)
                    print("remove", APPDELEGATE.arrSelect)
                    break
                }
            }
        }
    }
    
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return aryAttributedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let typeCell : FilterTypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! FilterTypeCollectionViewCell
        
        //CLEAR
        if objFilterVC.isClear == true {
            typeCell.isSelect = false
            typeCell.labelTitle.setTextLabelDynamicTextColor()
            typeCell.labelTitle.backgroundColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        }
        
        //MULTIPLE SELECTION
        let dictData : [String : AnyObject] = aryAttributedData[indexPath.row] as! [String : AnyObject]
        
        let strAttID : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "z_attributetermid_pk")
        print(strAttID)
        
        if APPDELEGATE.arrSelect.contains(strAttID) {
            typeCell.isSelect = true
            typeCell.labelTitle.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
            typeCell.labelTitle.textColor = UIColor.white
        }
        else
        {
            typeCell.isSelect = false
            typeCell.labelTitle.setTextLabelDynamicTextColor()
            typeCell.labelTitle.backgroundColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        }
        
        typeCell.labelTitle.text = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "attributetermsname")
        
        return typeCell
    }
    
    //MARK: UICollectionViewFlowLayOut
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if IS_IPAD_DEVICE() {
            return CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH / 6) - 28, height: 60)
        }
        if IS_IPHONE_5_OR_5S() {
            return CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH / 4) - 20, height: 38)
        }
        return CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH / 4) - 28, height: 50)
    }
}
