//
//  WishListTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/14/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Kingfisher

class WishListTableViewCell: UITableViewCell {
    
    @IBOutlet var viewBack: UIView!
    @IBOutlet var imageViewItem: UIImageView!
    @IBOutlet var lableItemName: PICustomLableName!
    @IBOutlet var lableDescription: PICustomDescription!
    @IBOutlet var labelPrice: PICustomLableName!
    
    @IBOutlet var buttonAddToCart: PICustomButtonRoundedCornerBlack!
    @IBOutlet var buttonRemove: PICustomButtonRoundedCornerBlack!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUpData(data : [String : AnyObject]) {
        print(data)
        
        self.lableItemName.text = PIService.object_forKeyWithValidationForClass_String(dict: data, key: "name")
        self.lableDescription.text = PIService.object_forKeyWithValidationForClass_String(dict: data, key: "description")        
        
        let product_Price : String = PIService.object_forKeyWithValidationForClass_String(dict: data, key: "price")
        
        print(product_Price)
        
        if product_Price != "" {
            let strPrice = String(format: "%.2f", product_Price.toFloat()!)
            self.labelPrice.text = BazarBitCurrency.setPriceSign(strPrice: strPrice)
        }
        
        if let key = data["main_image"] {
            if (key as? [String : AnyObject]) != nil
            {
                print(key)
                var imgUrl:String = key["main_image"] as! String
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                let url1 = URL(string: imgUrl)
                self.imageViewItem.kf.setImage(with: url1)
            }
        }
    }
    
}
