//
//  variationListTableViewCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 24/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire

class variationListTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var lblVariationName: UILabel!
    @IBOutlet weak var Cltnvariation: UICollectionView!
    
    @IBOutlet weak var cltnvariationWidthConstraints: NSLayoutConstraint!
    var arrVariationData : [AnyObject] = []
    var arrdisableproductID : [String] = []
    var arrdisableattributeName : [String] = []
    
    var ProductID:String = ""
    var strFirstProductID : String = ""
    
    var productdetailvc:ProductDetailsVC!
    
    var arrDisable : [AnyObject] = [AnyObject]()
    var aryDefaultVariationData : [AnyObject] = []
    var isClicked : Bool = false
    
    var dictFirst:[String:AnyObject] = [:]
    var dictSecond:[String:AnyObject] = [:]
    
    var cnt : Int = 0
    var tblPosition : Int = 0
    
    var BlockCollectionViewDidSelectHandler:((_ isServiceCall : Bool, _ strProductId : String) -> Void)?
    
    override func awakeFromNib() {
        Cltnvariation.dataSource = self
        Cltnvariation.delegate = self
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: SETUP DATA
    func setvariationData(dictModelDataVariation : [String:AnyObject], arrDisableData : [AnyObject])
    {
        if (dictModelDataVariation["terms"] as? [AnyObject]) != nil
        {
            arrVariationData = dictModelDataVariation["terms"] as! [AnyObject]

            if Constants.DeviceType.IS_IPAD {
              if (CGFloat(self.arrVariationData.count * 125)) > SCREEN_WIDTH - 40 {
                self.cltnvariationWidthConstraints.constant = SCREEN_WIDTH - 40
              }else {
                self.cltnvariationWidthConstraints.constant = CGFloat(self.arrVariationData.count * 125)
              }
            }else {
              if (CGFloat(self.arrVariationData.count * 80)) > SCREEN_WIDTH - 16 {
                self.cltnvariationWidthConstraints.constant = SCREEN_WIDTH - 16
              }else {
                self.cltnvariationWidthConstraints.constant = CGFloat(self.arrVariationData.count * 80)
              }
            }
        }
        
        self.arrDisable = []
        self.arrDisable = arrDisableData
        self.Cltnvariation.reloadData()
    }
    
    func setDefaultvariationData(aryModelDataDefaultVariation: [AnyObject]) {
        self.aryDefaultVariationData = aryModelDataDefaultVariation
    }
    
    // MARK: - UICOLLECTION VIEW METHODS
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrVariationData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let variationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cltnvariation", for: indexPath) as! VariationListCollectionViewCell
        
        
        //==================DISPLAY VARIATION====================
        let dict:[String:AnyObject] = arrVariationData[indexPath.row] as! [String:AnyObject]
        
        //print(dict)
        
        variationCell.strVariation = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
        variationCell.position = tblPosition
        
        ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_productid_fk")
        let attributetermid : String =  PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_attributetermid_fk")
        let attributeid : String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_attributeid_pk")
        let attributename = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "attritem_name")
        
        //        print(productdetailvc.arrAttributeID)
        //        print(productdetailvc.arrTermsAttributeID)
        
        if productdetailvc.arrAttributeID.contains(attributeid) &&
            productdetailvc.arrTermsAttributeID.contains(attributetermid)
        {
            let myNormalAttributedTitle = NSAttributedString(string: attributename,
                                                             attributes: [NSForegroundColorAttributeName : UIColor.white])
            
            variationCell.btnVariationName.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            variationCell.btnVariationName.setBackgroundColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!, forState: UIControlState.normal)
        }else {
            let myNormalAttributedTitle = NSAttributedString(string: attributename,
                                                             attributes: [NSForegroundColorAttributeName : UIColor(hexString: Constants.THEME_PRIMARY_COLOR) ?? "nil"])
            
            
            variationCell.btnVariationName.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            variationCell.btnVariationName.setBackgroundColor(UIColor.clear, forState: UIControlState.normal)
            
            variationCell.btnVariationName.layer.masksToBounds = false
            variationCell.btnVariationName.layer.cornerRadius = 10.0
            variationCell.btnVariationName.clipsToBounds = true
            variationCell.btnVariationName.layer.borderWidth = 1
            variationCell.btnVariationName.layer.borderColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)?.cgColor
        }
        
        variationCell.btnVariationName.tag = indexPath.row
        variationCell.btnVariationName.addTarget(self, action: #selector(self.btnVariationNameClicked), for: .touchUpInside)
        
        
        return variationCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if Constants.DeviceType.IS_IPAD {
            return CGSize(width: 90.0, height:50.0)
        } else {
            return CGSize(width: 70.0, height:30.0)
        }
    }
    
    // MARK:- HELPER METHOD
    func btnVariationNameClicked(sender:UIButton) {
        
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.Cltnvariation)
        let indexPath: IndexPath = self.Cltnvariation.indexPathForItem(at: buttonPosition)!
        
        let cell : VariationListCollectionViewCell = Cltnvariation.cellForItem(at: indexPath) as! VariationListCollectionViewCell
        
        let dict:[String:AnyObject] = arrVariationData[indexPath.row] as! [String:AnyObject]
        
        print(dict)
        
        productdetailvc.arrAttributeID[cell.position] = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_attributeid_pk")
        productdetailvc.arrTermsAttributeID[cell.position] = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_attributetermid_fk")
        
        APPDELEGATE.TermsProductId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "z_productid_fk")
        
        self.productdetailvc.defaultvariationCount = productdetailvc.arrAttributeID.count
        self.productdetailvc.DisableVariationOptionService(aryAttributeId: productdetailvc.arrAttributeID, aryAttributeTermsId: productdetailvc.arrTermsAttributeID)
    }
}
