//
//  AITextField.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import UIKit

class PICustomTextField: UITextField, UITextFieldDelegate, UIPickerViewDelegate {
    
    @IBInspectable var enableWhiteLine : Bool = false
    
    let viewSeparator : UIView = UIView()
    var minLength : Int = 0
    var maxLength : Int = 256
    var strValidationMessage : String = ""
    var strPlaceHolder : String = ""
    var strSelectedDate : String = ""
    var strGender : String = "Male"
    
    var textFieldType : AITextFieldType = .Normal
    var selectedDate  : NSDate!
    let datePickerView  : UIDatePicker = UIDatePicker()
    
    var TextFieldShouldReturnHandler:((_ textField: UITextField) -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.appFont_Light_WithSize(16)
        self.delegate = self
        self.autocorrectionType = UITextAutocorrectionType.no
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(useUnderline), userInfo: nil, repeats: false)
    }
    
    //MARK: USE UNDERLINE
    func useUnderline() {
        
        if self.enableWhiteLine == true {
            self.textColor = UIColor.white
        }
        else {
            self.textColor = UIColor.black
        }
        
        let borderView = UIView()
        
        if self.enableWhiteLine == true {
            borderView.backgroundColor = UIColor.lightGray
        }
        else{
            borderView.backgroundColor = UIColor.lightGray
        }
        borderView.frame = CGRect(x: 0,y: self.frame.size.height - 1,width: self.frame.size.width,height: 0.5)
        self.addSubview(borderView)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
 
    
    func addToolBar(){
        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.BlackOpaque
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(PICustomTextField.donePressed))
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PICustomTextField.cancelPressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        self.inputAccessoryView = toolBar
    }
    
    func donePressed(){
        self.endEditing(true)
    }
    
    func cancelPressed(){
          self.endEditing(true)
    }
}
