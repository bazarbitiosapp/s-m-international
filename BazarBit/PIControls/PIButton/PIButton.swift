//
//  AIButton.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import UIKit

class PICustomButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 5.0
        self.titleLabel?.font = UIFont.appFont_Bold_WithSize(14)
        //self.backgroundColor = UIColor.white
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}


class PICustomButtonRoundedCorner: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if IS_IPAD_DEVICE() {
            self.layer.cornerRadius = 20.0
        }
        else
        {
            self.layer.cornerRadius = 20.0
        }
        
        self.titleLabel?.font = UIFont.appFont_Bold_WithSize(14)
        self.backgroundColor = UIColor.white
        self.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
        //self.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.applyShadowDefault()
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

class PICustomButtonRoundedCornerBlack: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if IS_IPAD_DEVICE() {
            self.layer.cornerRadius = 30.0
        }
        else
        {
            self.layer.cornerRadius = 20.0
        }
        self.titleLabel?.font = UIFont.appFont_Bold_WithSize(14)
        //self.titleLabel?.textColor = UIColor.white
        self.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
        self.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.applyShadowDefault()
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

class PIFilterRounded: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if IS_IPAD_DEVICE() {
            self.layer.cornerRadius = 37.0
        }
        else
        {
            self.layer.cornerRadius = 25.0
        }
        
        self.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.applyShadowDefault()
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

class PIClearButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setButtonTextThemeColor()
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

class PIApplyButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setButtonDynamicColor()
        self.titleLabel?.font = UIFont.appFont_Bold_WithSize(16)
        //self.titleLabel?.textColor = UIColor.white
        self.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        self.setTitleColor(UIColor(hexString: Constants.BUTTON_TEXT_COLOR), for: .normal)
        self.applyShadowDefault()
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
    }
}

class PIFilterButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setButtonDynamicColor()
        self.backgroundColor = UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 220.0/255.0, alpha: 1.0)
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.layer.cornerRadius = 5.0
        self.titleLabel?.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PIFeedbackButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setTitleColor(UIColor(hexString: "076f30"), for: .normal)
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.font = UIFont.appFont_Light_WithSize(14)
    }
}

class PILogoutButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.setTitleColor(UIColor(hexString: Constants.TEXT_LIGHT_COLOR), for: .normal)
        
        let btnLogoutAttributedTitle = NSAttributedString(string: "Logout",
                                                      attributes: [NSForegroundColorAttributeName : UIColor(hexString: Constants.TEXT_LIGHT_COLOR)!])
        self.setAttributedTitle(btnLogoutAttributedTitle, for: .normal)
        
        self.backgroundColor = UIColor.clear
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.font = UIFont.appFont_Light_WithSize(20)
    }
}



