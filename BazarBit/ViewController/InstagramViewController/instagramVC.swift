//
//  instagramVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 03/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import InstagramKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Toaster

class instagramVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    var socialParameter: Parameters = [:]
    
    var instagramDetail:[String: AnyObject] = [:]
    var socialType: String = ""
    var socialId: String = ""
    var socialName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Instagram
        let url:URL = InstagramEngine.shared().authorizationURL()
        let requestObj = URLRequest(url: url)
        webView.loadRequest(requestObj)
        webView.scrollView.isScrollEnabled = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIWebViewDelegate Method
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if ((try? InstagramEngine.shared().receivedValidAccessToken(from: request.url!)) != nil) {
            
            let range: Range<String.Index> = (request.url?.absoluteString.range(of: "#access_token=")!)!
            let instagramAccessToken: String = (request.url?.absoluteString.substring(from: range.upperBound))!
            
            getInstagramUserData(strToken: instagramAccessToken)
        }
        return true
    }
    
    // Get Instagram user Data (Fixed)
    func getInstagramUserData(strToken: String)  {
        
        let strurl: String = BazarBit.getInstagramUserService(strAccessToken: strToken)
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let parameter: Parameters = [:]
        
        PIService.serviceCall(url: url, method: .get, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dict:[String: AnyObject] = responseDict
            
            let meta:[String: AnyObject] = dict["meta"] as! [String : AnyObject]
            
            let code: Int = meta["code"] as! Int
            
            if(code == 200) {
                
                let data:[String: AnyObject] = dict["data"] as! [String : AnyObject]
                self.instagramDetail = dict["data"] as! [String : AnyObject]
                
                self.socialId = PIService.object_forKeyWithValidationForClass_String(dict: data, key: "id")
                self.socialType = "instagram"
                self.socialName = PIService.object_forKeyWithValidationForClass_String(dict: self.instagramDetail, key: "full_name")
                
                self.socialParameter["social_id"] = self.socialId
                self.socialParameter["social_type"] = self.socialType
                self.socialLoginService()
                //print(data)
            }
        }
    }
    
    // MARK: - Social Service Method
    func socialLoginService() {
        
        let strurl: String = BazarBit.SocialMediaService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        PIService.serviceCall(url: url, method: .post, parameter: socialParameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            print(responseDict)
            print(self.socialParameter)
            let dict:[String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            PIAlertUtils.displayAlertWithMessage(message)
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
            
            if status == "ok" {
                
                if (dict["payload"] != nil) {
                    
                    let payload: [String: AnyObject] = dict["payload"] as! [String : AnyObject]
                    
                    UserDefaultFunction.setCustomDictionary(dict: payload, key: BazarBit.USERACCOUNT)
                    
                    let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                    
                    UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                    
                    let authoUser: [String: AnyObject] = payload["authUser"] as! [String : AnyObject]
                    UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                    
                    let strFName:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                    
                    UserDefaults.standard.set(strFName, forKey: BazarBit.USERNAME)
                    
                    let strEmail:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "username")
                    
                    UserDefaults.standard.set(strEmail, forKey: BazarBit.USEREMAILADDRESS)
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openHomePage()
                    }
                }
            }
            else {
                
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                
                if self.socialType == "instagram" {
                    registerVC.dictInstagram = self.instagramDetail
                }
                registerVC.socialId = self.socialId
                registerVC.socialType = self.socialType
                registerVC.socialName = self.socialName
                
                self.navigationController?.pushViewController(registerVC, animated: true)
            }
        }
    }
    
    // MARK:- UIACTION METHODS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
