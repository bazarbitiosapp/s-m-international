//
//  ChangePasswordViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/24/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txtOldPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    
    var strOldPassword: String = ""
    var strNewPassword: String = ""
    var strConfirmPassword: String = ""
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var btnChangePassword: UIButton!
    
    // MARK: - UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setBorderPropertiesForTextField()
        // Do any additional setup after loading the view.
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextField Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        if textField == txtOldPassword {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
        }  else if textField == txtNewPassword {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
        }  else if textField == txtConfirmPassword {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
        }
        
        return true
    }
    // MARK: - Action Method
    
    @IBAction func btnBack(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangePassword(_ sender: Any) {
        if checkValidation() {
            callService_ChangePassword()
        }
    }
    
    // MARK: - Service Method
    func callService_ChangePassword()  {
        
        let strurl: String = BazarBit.changePasswordService()
        let url: URL = URL(string: strurl)!
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["password"] = strOldPassword
        parameter["newpassword"] = strNewPassword
        parameter["confirmpassword"] = strConfirmPassword
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            
            let dictResponse: [String: AnyObject] = responseDict
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                self.txtOldPassword.text = ""
                self.txtNewPassword.text = ""
                self.txtConfirmPassword.text = ""
                //APPDELEGATE.openLoginPage()
                self.ServiceCallForLogOut()
                PIAlertUtils.displayAlertWithMessage(message)
            } else {
                
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: ServiceCallForLogOut
    func ServiceCallForLogOut() {
        let strurl: String = BazarBit.logoutService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter: Parameters = [:]
        
        parameter["user_token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN)
        
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            
            let statusCode: Int = BazarBit.getValue(dict: responseDict, key: "code")
            
            print(status)
            
            if statusCode == 401 || statusCode == 200 {
                
                if (responseDict["payload"] != nil) {
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERACCOUNT)
                    UserDefaults.standard.removeObject(forKey: BazarBit.AUTHTOKEN)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERID)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERNAME)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERPROFILEIMAGE)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    
                    UserDefaults.standard.removeObject(forKey: "slug")
                    UserDefaults.standard.removeObject(forKey: "title")
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        UserDefaults.standard.removeObject(forKey: BazarBit.AUTOPINCODE)
                        myDelegate.openLoginPage()
                        
                    }
                }
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    // MARK: - Helper Method
    func setBorderPropertiesForTextField()  {
        txtOldPassword.setLeftPaddingPoints(10.0)
        txtOldPassword.setTextFieldDynamicColor()
        txtNewPassword.setLeftPaddingPoints(10.0)
        txtNewPassword.setTextFieldDynamicColor()
        txtConfirmPassword.setLeftPaddingPoints(10.0)
        txtConfirmPassword.setTextFieldDynamicColor()
        btnChangePassword.layer.cornerRadius = 20.0
        btnChangePassword.setButtonDynamicColor()
        btnChangePassword.Shadow()
    }
    
    func checkValidation() -> Bool {
        var isValid: Bool = true
        strOldPassword = txtOldPassword.text!
        strOldPassword = strOldPassword.trimmed()
        strNewPassword = txtNewPassword.text!
        strNewPassword = strNewPassword.trimmed()
        strConfirmPassword = txtConfirmPassword.text!
        strConfirmPassword = strConfirmPassword.trimmed()
        if PIValidation.isBlankString(str: strOldPassword)  {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter Old password!")
            return isValid
            
        }  else if strOldPassword.characters.count < 6 || strOldPassword.characters.count > 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter old password between 6 to 30 character!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strNewPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter new Password!")
            return isValid
            
        }  else if strNewPassword.characters.count < 6  || strNewPassword.characters.count > 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter new password between 6 to 30 character!")
            return isValid
        } else if PIValidation.isBlankString(str: strConfirmPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter confirm Password!")
            return isValid
            
        } else if strConfirmPassword.characters.count < 6 || strConfirmPassword.characters.count > 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter confirm password between 6 to 30 character!")
            return isValid
        } else if PIValidation.isCompareTwoStringEqual(firstStr: strNewPassword, secondStr: strConfirmPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Confirm Password Not Match...!")
            return isValid
        }
        else if !PIValidation.isCompareTwoStringEqual(firstStr: strOldPassword, secondStr: strNewPassword) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("New password should not be same as old password...!")
            return isValid
        }
        return isValid
    }
}
