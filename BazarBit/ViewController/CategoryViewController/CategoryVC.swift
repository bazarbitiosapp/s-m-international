//
//  CategoryVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 09/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import EZSwiftExtensions

class CategoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tabbar: TabbarVC!
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var imageViewLogo : UIImageView!
    @IBOutlet var tableViewMegaMenu : UITableView!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var lblCartTotalCount: UILabel!
    @IBOutlet weak var lblCartTotalWidthConstraints: NSLayoutConstraint!
    
    var identifier : String = "menuCell"
    var aryMenuList : [AnyObject] = [AnyObject]()
    var previousIndex : IndexPath = IndexPath(row: -1, section: 0)
    var dicResponceCartTotalList:[String:AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        self.tableViewMegaMenu.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        self.tableViewMegaMenu.tableFooterView = UIView()
        
        // Main Logo Image set
        var LogoImage:String = APPDELEGATE.applicationImage
        LogoImage = LogoImage.replacingOccurrences(of: " ", with: "%20")
        let url = URL(string: LogoImage)
        imageViewLogo.kf.setImage(with: url)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        lblCartTotalCount.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.GetCartCountData()
        self.ServiceCallForGetMegaMenu()
    }
    
    //MARK: UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if IS_IPAD_DEVICE() {
            return 54.0
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            
            var dictData = aryMenuList[indexPath.row] as? [String: AnyObject] ?? [String: AnyObject]()
            
            if (dictData["category_id"] as? String) != nil {
                let objMegaMenuListVC : MegaMenuIteListViewController = self.storyboard?.instantiateViewController(withIdentifier: "MegaMenuIteListViewController") as! MegaMenuIteListViewController
                
                objMegaMenuListVC.strSlug = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "slug")
                objMegaMenuListVC.strTitle = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "name")
                
                self.navigationController?.pushViewController(objMegaMenuListVC, animated: true)
            }
            
            if (dictData["child"] as? [AnyObject]) != nil || (dictData["subcat"] as? [AnyObject]) != nil  {
                
                var aryTemp: [AnyObject] = []
                
                if (dictData["child"] as? [AnyObject]) != nil {
                    aryTemp = (dictData["child"] as? [AnyObject])!
                    
                    let cell : MegaMenuTableViewCell = tableView.cellForRow(at: indexPath) as! MegaMenuTableViewCell
                    
                    cell.lableTitle.font = UIFont.appFont_Light_WithSize(20)
                    cell.lableTitle.textAlignment = .center
                }
                
                if (dictData["subcat"] as? [AnyObject]) != nil {
                    aryTemp = (dictData["subcat"] as? [AnyObject])!
                    print(aryTemp)
                    
                    let cell : MegaMenuTableViewCell = tableView.cellForRow(at: indexPath) as! MegaMenuTableViewCell
                    cell.lableTitle.font = UIFont.boldSystemFont(ofSize: 16)
                    cell.backgroundColor = UIColor.clear
                    cell.lableTitle.textAlignment = .left
                    
                    if previousIndex.row != -1 && previousIndex != nil && previousIndex.row <= aryMenuList.count - 1 {
                        let cell : MegaMenuTableViewCell = tableView.cellForRow(at: previousIndex) as! MegaMenuTableViewCell
                        cell.lableTitle.font = UIFont.appFont_Light_WithSize(16)
                    }
                    previousIndex = indexPath
                }
                
                if aryTemp.count > 0 {
                    var isAlreadyInserted: Bool = false
                    
                    for i in 0...aryTemp.count - 1 {
                        
                        let dictInner: [String: AnyObject] = aryTemp[i] as! [String : AnyObject]
                        
                        let index = getIndexValue(k: aryMenuList, dictSearch: dictInner)
                        
                        isAlreadyInserted = (index > 0 && index != NSIntegerMax)
                        
                        if isAlreadyInserted {
                            break
                        }
                    }
                    
                    let cell : MegaMenuTableViewCell = tableView.cellForRow(at: indexPath) as! MegaMenuTableViewCell
                    
                    if isAlreadyInserted {
                        
                        if (dictData["subcat"] as? [AnyObject]) != nil {
                            cell.imageViewArrow.image = UIImage(named: "down-arrow")
                        }
                        miniMizeRows(arrData: aryTemp)
                    }
                    else {
                        
                        if (dictData["subcat"] as? [AnyObject]) != nil {
                            cell.imageViewArrow.image = UIImage(named: "up-arrow")
                        }
                        
                        var count: Int = indexPath.row + 1
                        var arCells = [Any]()
                        
                        print(aryTemp.count)
                        
                        for i in 0...aryTemp.count - 1 {
                            let dInner: [String: AnyObject] = aryTemp[i] as! [String : AnyObject]
                            arCells.append(IndexPath(row: count, section: 0))
                            
                            aryMenuList.insert(dInner as AnyObject, at: count)
                            count += 1
                        }
                        tableView.insertRows(at: arCells as? [IndexPath] ?? [IndexPath](), with: .left)
                    }
                }
            }
        }
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryMenuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuCell : MegaMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MegaMenuTableViewCell
        
        menuCell.contentView.backgroundColor = UIColor.clear
        menuCell.imageViewArrow.isHidden = true
        
        if indexPath.section == 0 {
            
            let dict: [String: AnyObject] = aryMenuList[indexPath.row] as! [String : AnyObject]
            
            if (dict["menuname"] as? String) != nil  {
                
                if indexPath.row % 2 == 0 {
                    menuCell.backgroundColor = UIColor.white
                }
                
                let strName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "menuname")
                menuCell.lableTitle.text = strName
                menuCell.lableTitle.textAlignment = .center
                menuCell.lableTitle.font = UIFont.appFont_Light_WithSize(20)
                
                menuCell.imageViewArrow.isHidden = false
                menuCell.imageViewArrow.image = UIImage(named: "down-arrow")
                menuCell.labelLeading.constant = 0.0
            }
            
            if (dict["categoryname"] as? String) != nil  {
                
                let strName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "categoryname")
                menuCell.lableTitle.text = strName
                menuCell.lableTitle.textAlignment = .left
                menuCell.backgroundColor = UIColor.clear
                menuCell.lableTitle.font = UIFont.appFont_Light_WithSize(16)
                menuCell.labelLeading.constant = 14.0
                
                if let arrSubCat = dict["subcat"]
                {
                    if arrSubCat.count > 0 {
                        menuCell.imageViewArrow.isHidden = false
                        menuCell.imageViewArrow.image = UIImage(named: "down-arrow")
                    }
                }
            }
            
            if (dict["name"] as? String) != nil  {
                
                let strName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
                menuCell.lableTitle.text = strName
                menuCell.lableTitle.textAlignment = .left
                menuCell.backgroundColor = UIColor.clear
                menuCell.lableTitle.font = UIFont.appFont_Light_WithSize(16)
                menuCell.labelLeading.constant = 28.0
                menuCell.imageViewArrow.isHidden = true
            }
        }
        return menuCell
    }
    
    // MARK:- ACTION METHOD
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    // MARK:- btnShoppingCart
    @IBAction func btnShoppingCart(_ sender: Any) {
        
        let shoppingCartViewController : ShoppingCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShoppingCartViewController") as! ShoppingCartViewController
        
        self.navigationController?.pushViewController(shoppingCartViewController, animated: true)
    }
    
    
    //MARK: ServiceCallForGetMegaMenu
    func ServiceCallForGetMegaMenu() {
        
        let strurl:String = BazarBit.GetMegaMenuService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        if let authToken = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN)
        {
            headers["auth-token"] = authToken as? String
        }
        else
        {
            headers["auth-token"] = ""
        }
        print(headers)
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                self.aryMenuList = []
                if (response["payload"] != nil)
                {
                    let payload:[AnyObject] = response["payload"] as! [AnyObject]
                    
                    for dict in payload
                    {
                        print(dict)
                        let megaMenuId : String = PIService.object_forKeyWithValidationForClass_String(dict: dict as! [String : AnyObject], key: "z_megamenuid_fk")
                        print(megaMenuId)
                        
                        if megaMenuId != "0"
                        {
                            self.aryMenuList.append(dict)
                        }
                    }
                    self.tableViewMegaMenu.dataSource = self
                    self.tableViewMegaMenu.delegate = self
                    self.tableViewMegaMenu.reloadData()
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func GetCartCountData() {
        
        let strurl:String = BazarBit.CountCartTotalService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        if UserId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else
        {
            var strSessionId:String = BazarBit.getSessionId()
            parameter["session_id"] = strSessionId
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCartTotalList = responseDict
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCartTotalList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
                    print(payload)
                    
                    let strCartTotal: String = payload["cart_items"] as! String
                    UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
                    
                    if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
                        
                        let strCartCount: String = UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) as! String
                        
                        if strCartCount != "0" {
                            print(strCartCount.characters.count)
                            if strCartCount.characters.count == 4 {
                                self.lblCartTotalWidthConstraints.constant = 30
                            } else if strCartCount.characters.count == 3  {
                                self.lblCartTotalWidthConstraints.constant = 25
                            }  else {
                                self.lblCartTotalWidthConstraints.constant = 18
                            }
                            
                            self.lblCartTotalCount.isHidden = false
                            self.lblCartTotalCount.text = strCartCount
                            self.lblCartTotalCount.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
                            self.lblCartTotalCount.isHidden = false
                        } else {
                            self.lblCartTotalCount.isHidden = true
                        }
                        
                        if Constants.DeviceType.IS_IPAD {
                            self.lblCartTotalCount.layer.masksToBounds = true
                            self.lblCartTotalCount.layer.cornerRadius = 10
                        }
                        else
                        {
                            self.lblCartTotalCount.layer.masksToBounds = true
                            self.lblCartTotalCount.layer.cornerRadius = 10
                        }
                        if APPDELEGATE.is_cart_active == "0"
                        {
                            if Constants.DeviceType.IS_IPAD {
                                self.cartView.isHidden = true
                            }else {
                                self.cartView.isHidden = true
                            }
                        }else {
                            if Constants.DeviceType.IS_IPAD {
                                self.cartView.isHidden = false
                            }else {
                                self.cartView.isHidden = false
                            }
                        }
                    }
                }
            }
        })
    }
    
    // MARK: - Helper Method
    func miniMizeRows(arrData: [AnyObject]) {
        
        if arrData.count >= 0 {
            for i in 0...arrData.count - 1  {
                
                let dInner: [String: AnyObject] = arrData[i] as! [String : AnyObject]
                let indexToRemove = getIndexValue(k: aryMenuList, dictSearch: dInner)
                
                if (dInner["child"] as? [AnyObject]) != nil   ||  (dInner["subcat"] as? [AnyObject]) != nil   {
                    
                    var arrInner: [AnyObject] = []
                    
                    if dInner["child"]  != nil {
                        arrInner = (dInner["child"] as? [AnyObject])!
                    }
                    
                    if dInner["subcat"]  != nil {
                        arrInner = (dInner["subcat"] as? [AnyObject])!
                    }
                    
                    if arrInner.count > 0 {
                        miniMizeRows(arrData: arrInner)
                    }
                    
                    if indexToRemove > 0  {
                        
                        self.aryMenuList.remove(at: indexToRemove)
                        tableViewMegaMenu.deleteRows(at: [IndexPath(row: indexToRemove, section: 0)], with: .fade)
                    }
                } else {
                    let dInner: [String: AnyObject] = arrData[i] as! [String : AnyObject]
                    let indexToRemove = getIndexValue(k: aryMenuList, dictSearch: dInner)
                    
                    if indexToRemove > 0 {
                        self.aryMenuList.remove(at: indexToRemove)
                        tableViewMegaMenu.deleteRows(at: [IndexPath(row: indexToRemove, section: 0)], with: .fade)
                    }
                }
            }
        }
    }
    
    func getIndexValue(k: [AnyObject], dictSearch: [String: AnyObject]) -> Int {
        
        let index = k.index {
            if let dic = $0 as? Dictionary<String,AnyObject> {
                
                if( NSDictionary(dictionary: dic).isEqual(to: dictSearch) )  {
                    return true
                }
            }
            return false
        }
        if index == nil {
            return -1
        } else {
            return index!
        }
    }
}
