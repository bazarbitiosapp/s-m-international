//
//  CMSViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CMSViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    @IBOutlet var textViewDescription: PICustomTextView!
    @IBOutlet var scrollViewCMS: UIScrollView!

    var strTitle : String = ""
    var strSlug : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        self.view.backgroundColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        self.textViewDescription.backgroundColor = UIColor.clear
        
        strSlug = UserDefaults.standard.object(forKey: "slug") as! String
        strTitle = UserDefaults.standard.object(forKey: "title") as! String
        
        self.lableTitle.text = strTitle.capitalized
        
        self.ServiceCallForLoadStaticPage(strSlug: strSlug)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return false
    }
    
    // MARK:- ACTION METHOD
    
    func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }

    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    //MARK: ServiceCallForLoadStaticPage
    func ServiceCallForLoadStaticPage(strSlug : String) {
        
        let strurl:String = BazarBit.LoadStaticPageService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["page"] = strSlug
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (responseDict["payload"] != nil)
                {
                    let dictData : [String: AnyObject] = responseDict["payload"] as! [String: AnyObject]
                    
                    print(dictData)
                    
                    let htmlString:String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "description")
                    let attributedString = self.stringFromHtml(string: htmlString)
                    self.textViewDescription.text = attributedString?.string
               //     self.textViewDescription.font = UIFont(name: "calibri", size: 12.0)!
                    
                }
            }
                
            else
            {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
}


