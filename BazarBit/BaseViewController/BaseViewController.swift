//
//  BaseViewController.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import UIKit
class BaseViewController: UIViewController {
    
    var backButtonType: AIBackButtonType = AIBackButtonType.BlackArrow

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.doSetupBackButtonType()
        
        switch self.backButtonType {
        case .BlackArrow:
//            let button   = UIButton(type: UIButtonType.custom) as UIButton
//            button.frame = CGRect(x: 5,y: 0,width: 40,height: 35)
//            button.setImage(UIImage(named: "back-icon"), for: .normal)
//            button.addTarget(self, action: #selector(BaseViewController.backButtonTapHandler(_:)), for:.TouchUpInside)
//            let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//            negativeSpace.width = -15.0
//            let item1:UIBarButtonItem = UIBarButtonItem(customView: button)
//            self.navigationItem.leftBarButtonItems = [negativeSpace,item1]
            break
        case .Cross:
            break;
        case .WhiteArrow:
            break;
        case .None:
            break;
        default:
            break;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- BACK BUTTON PRESS HANDLERS
    func backButtonTapHandler(sender:UIButton) {
        
        switch self.backButtonType {
        case .BlackArrow:
            self.navigationController?.popViewController(animated: true)
        case .WhiteArrow:
            self.navigationController?.popViewController(animated: true)
        case .Cross:
            self.navigationController?.popViewController(animated: true)
        case .None:
            break
        }
    }
    
    //MARK: DO SETUP BACK BUTTON TYPE
    func doSetupBackButtonType(){
        switch self {
        case is LoginVC: break;
//            self.backButtonType = AIBackButtonType.BlackArrow
//            self.navigationItem.title = "Create New Account"
//            self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Lato-Bold", size: 16)!,  NSForegroundColorAttributeName: UIColor.black]
        default:
            break;
        }
    }
}
